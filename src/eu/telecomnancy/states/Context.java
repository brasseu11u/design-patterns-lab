package eu.telecomnancy.states;

public class Context{
	private IState state;
	public static StateOn on = new StateOn();
	public static StateOff off = new StateOff();

	public Context() {
		this.setState(null);
	}
	
	public IState getState() {
		return state;
	}
	
	public void setState(IState state) {
		this.state = state;
	}

}
