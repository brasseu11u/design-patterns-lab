package eu.telecomnancy.states;

public interface IState {
	
	/**
	 * Set the new state of the context
	 * 
	 * @param context
	 */
	public void set(Context context);

}
