package eu.telecomnancy.ui;

import eu.telecomnancy.observers.IObserver;
import eu.telecomnancy.sensor.DecoratorSensor;

public abstract class DecoratedSensorView extends DecoratorSensor implements IObserver {
	protected DecoratorSensor decoratedSensor;
	protected SensorView decoratedSensorView;
	
	public DecoratedSensorView(DecoratorSensor sensor) {
		this.decoratedSensor = sensor;
		this.decoratedSensorView = new SensorView(this.decoratedSensor);
	}
	
}
