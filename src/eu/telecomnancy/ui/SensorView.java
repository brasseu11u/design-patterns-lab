package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SensorView extends JPanel implements eu.telecomnancy.observers.IObserver {
    private ISensor sensor;
    
    private boolean rounding = false;
    private char unit = 'C';
    private JLabel value = new JLabel("N/A °"+unit);
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            //@Override
            public void actionPerformed(ActionEvent e) {
                sensor.on();
            }
        });

        off.addActionListener(new ActionListener() {
            //@Override
            public void actionPerformed(ActionEvent e) {
                sensor.off();
            }
        });

        update.addActionListener(new ActionListener() {
            //@Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sensor.update();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                    System.exit(ERROR);
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }
    
    /**
     * Get update from observed subject
     * 
     * @throws SensorNotActivatedException 
     */
    //@Override
	public void update() throws SensorNotActivatedException {
		try {
			double value = sensor.getValue();
			if (this.unit == 'F') value = 1.8*value+32;
			if (this.rounding) value = (int) value;
			this.value.setText(""+value+"�"+unit);
		} catch (SensorNotActivatedException sensorNotActivatedException) {
			sensorNotActivatedException.printStackTrace();
		}
	}
	
	/**
	 * Set unit to desired unit C or F
	 * else no-op
	 */
	public void setUnit(char unit) {
		if (unit == 'C' || unit == 'F')	this.unit = unit;
	}
	
	/**
	 * Set up rounding
	 * 
	 * @param rounding
	 */
	public void setRounding(boolean rounding) {
		this.rounding = rounding;
	}

}
