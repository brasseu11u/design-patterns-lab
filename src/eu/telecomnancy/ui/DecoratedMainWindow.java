package eu.telecomnancy.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import eu.telecomnancy.decorators.FarenheitDecorator;
import eu.telecomnancy.decorators.RoundingDecorator;
import eu.telecomnancy.observers.IObserver;
import eu.telecomnancy.observers.Observable;
import eu.telecomnancy.sensor.DecoratorSensor;
import eu.telecomnancy.sensor.ISensor;

public class DecoratedMainWindow extends JFrame {

    private ISensor sensor;
    private DecoratedSensorView sensorView;

    public DecoratedMainWindow(ISensor sensor) {
        this.sensor = sensor;
        this.sensorView = new FarenheitDecorator(new RoundingDecorator((DecoratorSensor) this.sensor));
        IObserver observer = this.sensorView;
		((Observable)this.sensor).addObserver(observer);

        this.setLayout(new BorderLayout());
        this.add(this.sensorView.decoratedSensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}
