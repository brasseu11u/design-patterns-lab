package eu.telecomnancy.sensor;

import eu.telecomnancy.sensor.LegacyTemperatureSensor;

public class AdapterLegacyTemperatureSensor implements ISensor {
	ISensor ISensor;
	LegacyTemperatureSensor LegacyTemperatureSensor;
	
	public AdapterLegacyTemperatureSensor() {
		LegacyTemperatureSensor = new LegacyTemperatureSensor();
	}

	public void on() {
		if (this.getStatus() == false) {
			this.LegacyTemperatureSensor.onOff();
		}
	}

	public void off() {
		if (this.getStatus() == true) {
			this.LegacyTemperatureSensor.onOff();
		}
	}

	public boolean getStatus() {
		return this.LegacyTemperatureSensor.getStatus();
	}

	public void update() throws SensorNotActivatedException {
		if (this.LegacyTemperatureSensor.getStatus() == false) {
			throw new SensorNotActivatedException("LegacyTemperatureSensor not activated");	
		}
	}

	public double getValue() throws SensorNotActivatedException {
		try {
			this.update();
		}
		catch (SensorNotActivatedException e) {
			throw e;
		};
		
		return this.LegacyTemperatureSensor.getTemperature();
	}

}
