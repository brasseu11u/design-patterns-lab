package eu.telecomnancy.sensor;

import java.util.*;

public class ProxyTemperatureSensor implements ISensor {
	private ContextTemperatureSensor realSensor = new ContextTemperatureSensor();
	private Date currentDate;
	
	public ContextTemperatureSensor getRealSensor() {
		return realSensor;
	}

	public void setRealSensor(ContextTemperatureSensor realSensor) {
		this.realSensor = realSensor;
	}

	public void on() {
		currentDate = new Date();
		this.realSensor.on();
		System.out.println(""+currentDate+" : TemperatureSensor.on()");
	}

	public void off() {
		currentDate = new Date();
		this.realSensor.off();
		System.out.println(""+currentDate+" : TemperatureSensor.off()");
	}

	public boolean getStatus() {
		currentDate = new Date();
		boolean status = this.realSensor.getStatus();
		System.out.println(""+currentDate+" : TemperatureSensor.getStatus() : "+status);
		return status;
	}

	public void update() throws SensorNotActivatedException {
		boolean failed = false;
		currentDate = new Date();
		try {
			this.realSensor.update();
		}
		catch (SensorNotActivatedException SENSOR_NOT_ACTIVATED) {
			System.out.println(""+currentDate+" : TemperatureSensor.update() failed : "+SENSOR_NOT_ACTIVATED);
			failed = true;
		}
		if (!failed) System.out.println(""+currentDate+" : TemperatureSensor.update()");
	}

	public double getValue() throws SensorNotActivatedException {
		currentDate = new Date();
		boolean failed = false;
		double value = 0;
		try {
			value = this.realSensor.getValue();
		}
		catch (SensorNotActivatedException SENSOR_NOT_ACTIVATED){
			System.out.println(""+currentDate+" : TemperatureSensor.getValue() failed : "+SENSOR_NOT_ACTIVATED);
			failed = true;
		}
		if (!failed) System.out.println(""+currentDate+" : TemperatureSensor.getValue() : "+value);
		return value;
	}

}
