package eu.telecomnancy.sensor;

import java.util.Random;

import eu.telecomnancy.states.*;

public class ContextTemperatureSensor implements ISensor {
    private Context context = new Context();
    double value = 0;

    //@Override
    public void on() {
        this.context.setState(Context.on);
    }

    //@Override
    public void off() {
    	this.context.setState(Context.off);
    }

    //@Override
    public boolean getStatus() {
    	if (this.context.getState() != null)
    		return this.context.getState() == (Context.on);
    	else
    		return false;
    }

    //@Override
    public void update() throws SensorNotActivatedException {
        if (this.getStatus() == true) {
            value = (new Random()).nextDouble() * 100;
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    //@Override
    public double getValue() throws SensorNotActivatedException {
        if (this.getStatus() == true)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
