package eu.telecomnancy.observers;

import java.util.ArrayList;
import java.util.Iterator;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Observable {
	private ArrayList<IObserver> m_observers;
	
	public Observable() {
		this.m_observers = new ArrayList<IObserver>();
	}
	
	/**
	 * Notify all observers
	 * 
	 * @throws SensorNotActivatedException 
	 */
	public void notifyObservers() throws SensorNotActivatedException {
		Iterator<IObserver> iter = this.m_observers.iterator();
		while(iter.hasNext()){
			IObserver obs = iter.next();
			obs.update();
		}
	}
	
	/**
	 * Add new observer at beginning of the list
	 * 
	 * @param observer
	 */
	public void addObserver(IObserver observer) {
		this.m_observers.add(observer);
	}
	
	/**
	 * Remove observer from the list
	 * 
	 * @param observer
	 */
	public void removeObserver(IObserver observer) {
		this.m_observers.remove(observer);
	}
}
