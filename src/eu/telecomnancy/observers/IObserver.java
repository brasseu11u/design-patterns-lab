package eu.telecomnancy.observers;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface IObserver {	
	/**
	 * Update the observer
	 * 
	 * @throws SensorNotActivatedException 
	 */
	public void update() throws SensorNotActivatedException;
}
