package eu.telecomnancy.decorators;

import eu.telecomnancy.sensor.DecoratorSensor;
import eu.telecomnancy.ui.DecoratedSensorView;

public class RoundingDecorator extends DecoratedSensorView {

	public RoundingDecorator(DecoratorSensor sensor) {
		super(sensor);
		this.decoratedSensorView.setRounding(true);
	}

}
