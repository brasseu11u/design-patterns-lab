package eu.telecomnancy.decorators;

import eu.telecomnancy.sensor.DecoratorSensor;
import eu.telecomnancy.ui.DecoratedSensorView;

public class FarenheitDecorator extends DecoratedSensorView {

	public FarenheitDecorator(DecoratorSensor sensor) {
		super(sensor);
		this.decoratedSensorView.setUnit('F');
	}

}