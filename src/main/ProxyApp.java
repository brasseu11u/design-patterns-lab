package main;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ProxyTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class ProxyApp {

    public static void main(String[] args) {
        ISensor sensor = new ProxyTemperatureSensor();
        new ConsoleUI(sensor);
    }

}