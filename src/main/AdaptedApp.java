package main;

import eu.telecomnancy.sensor.AdapterLegacyTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AdaptedApp {
	AdapterLegacyTemperatureSensor Adapter;
	
    public static void main(String[] args) {
        ISensor sensor = new AdapterLegacyTemperatureSensor();
        new ConsoleUI(sensor);
    }
}
