package main;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ContextTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class StateApp {

    public static void main(String[] args) {
        ISensor sensor = new ContextTemperatureSensor();
        new ConsoleUI(sensor);
    }

}