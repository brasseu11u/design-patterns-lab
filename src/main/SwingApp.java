package main;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ObservedTemperatureSensor;
import eu.telecomnancy.ui.DecoratedMainWindow;

public class SwingApp {

    public static void main(String[] args) {
        ISensor sensor = new ObservedTemperatureSensor();
        new DecoratedMainWindow(sensor);
    }

}
